nagiosxi
=============

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with nagiosxi](#setup)
    * [What nagiosxi affects](#what-nagiosxi-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with nagiosxi](#beginning-with-nagiosxi)
4. [Usage - Configuration options and additional functionality](#usage)
    * [Customize the nagiosxi options](#customize-the-nagiosxi-options)
    * [Configure with hiera yaml](#configure-with-hiera-yaml)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
    * [Classes](#classes)
6. [Limitations - OS compatibility, etc.](#limitations)
7. [Development - Guide for contributing to the module](#development)
8. [Contributors](#contributors)

## Overview

Puppet Module to Install and set up nagiosxi using the source tar.gz

## Module Description

The nagiosxi module can download and uncompress the nagiosxi installer and
run the install or update script.  We can also manage the official mod geraman
install script.

## Setup

### What nagiosxi affects

* download the tar ball and extract to /opt/source
* run the install or update script
* install mod gearman on server and workers

### Setup Requirements

only need to install the module

### Beginning with nagiosxi

Minimal nagiosxi setup:

```puppet
class { 'nagiosxi': }
```

## Usage

### Customize the nagiosxi options

Specify a specific version from [here](https://assets.nagios.com/downloads/nagiosxi/versions.php) and choose to upgrade.

```puppet
class { 'nagiosxi':
  major   => '2014',
  version => 'xi-2014r2.7',
  action  => 'upgrade',
}
```

### Configure with hiera yaml

```puppet
include nagiosxi
```
```yaml
---
nagiosxi::major: '2014'
nagiosxi::version: 'xi-2014r2.7'
```

## Reference

### Classes

* nagiosxi

## Limitations

This module has been built on and tested against Puppet 3.8.x and higher.  
While I am sure other versions work, I have not tested them.

This module supports modern RedHat only systems.  
This module has been tested on CentOS 6.x.

No plans to support other versions (unless you add it :)..

## Development

Pull Requests welcome

## Contributors

Chris Edester (edestecd)
