require 'spec_helper'

describe 'nagiosxi::gearman::worker' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts.merge(environment: 'test', concat_basedir: '/var/lib/puppet/concat', staging_http_get: 'curl')
      end

      context 'with defaults' do
        let(:params) { { master_ip: '127.0.0.1', encryption_key: 'super_secret' } }

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_class('nagiosxi::gearman::worker::install') }
        it { is_expected.to contain_class('nagiosxi::gearman::worker::config') }
        it { is_expected.to contain_class('nagiosxi::gearman::worker::service') }

        context 'nagiosxi::gearman::worker::install' do
          it do
            case facts[:operatingsystemrelease]
            when '6'
              is_expected.to contain_archive('/opt/staging/nagiosxi/ModGearmanFullinstallVersionCore4.sh').with(
                source: 'https://assets.nagios.com/downloads/nagiosxi/scripts/ModGearmanFullinstallVersionCore4.sh',
              ).that_notifies('Exec[ModGearmanFullinstallVersionCore4.sh]')
            when '7'
              is_expected.to contain_archive('/opt/staging/nagiosxi/ModGearmanInstall.sh').with(
                source: 'https://assets.nagios.com/downloads/nagiosxi/scripts/ModGearmanInstall.sh',
              ).that_notifies('Exec[ModGearmanInstall.sh]')
            end
          end

          it do
            case facts[:operatingsystemrelease]
            when '6'
              is_expected.to contain_archive('/opt/staging/nagiosxi/ModGearmanFullinstallVersionCore4.sh').with(
                source: 'https://assets.nagios.com/downloads/nagiosxi/scripts/ModGearmanFullinstallVersionCore4.sh',
              ).that_notifies('File[/opt/staging/nagiosxi/ModGearmanFullinstallVersionCore4.sh]')
            when '7'
              is_expected.to contain_archive('/opt/staging/nagiosxi/ModGearmanInstall.sh').with(
                source: 'https://assets.nagios.com/downloads/nagiosxi/scripts/ModGearmanInstall.sh',
              ).that_notifies('File[/opt/staging/nagiosxi/ModGearmanInstall.sh]')
            end
          end

          it do
            case facts[:operatingsystemrelease]
            when '6'
              is_expected.to contain_file('/opt/staging/nagiosxi/ModGearmanFullinstallVersionCore4.sh')
                .with(
                  ensure: 'file',
                  mode:   '0755',
                )
            when '7'
              is_expected.to contain_file('/opt/staging/nagiosxi/ModGearmanInstall.sh')
                .with(
                  ensure: 'file',
                  mode:   '0755',
                )
            end
          end

          it do
            case facts[:operatingsystemrelease]
            when '6'
              is_expected.to contain_exec('ModGearmanFullinstallVersionCore4.sh').with(
                command:     "echo '127.0.0.1' | ModGearmanFullinstallVersionCore4.sh > ModGearmanFullinstallVersionCore4.log",
                cwd:         '/opt/staging/nagiosxi',
                refreshonly: true,
                timeout:     3600,
              )
            when '7'
              is_expected.to contain_exec('ModGearmanInstall.sh').with(
                command:     "echo '127.0.0.1' | ModGearmanInstall.sh > ModGearmanInstall.log",
                cwd:         '/opt/staging/nagiosxi',
                refreshonly: true,
                timeout:     3600,
              )
            end
          end
        end

        context 'nagiosxi::gearman::worker::config' do
          it { is_expected.to contain_nagiosxi__gearman__worker__config_entry('server') }
        end

        # rubocop:disable EmptyExampleGroup
        context 'nagiosxi::gearman::worker::service' do
          case facts[:operatingsystemrelease]
          when '6'
            it { is_expected.to contain_service('mod_gearman_worker') }
          when '7'
            it { is_expected.to contain_service('mod-gearman-worker') }
          end
        end
        # rubocop:enable EmptyExampleGroup
      end
    end
  end
end
