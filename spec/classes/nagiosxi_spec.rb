require 'spec_helper'

describe 'nagiosxi' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts.merge(environment: 'test', concat_basedir: '/var/lib/puppet/concat', staging_http_get: 'curl')
      end

      context 'with defaults' do
        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_class('nagiosxi::install') }
        it { is_expected.to contain_class('nagiosxi::ssl') }
        it { is_expected.to contain_class('nagiosxi::service') }
        it { is_expected.not_to contain_class('nagiosxi::gearman') }
      end

      context 'with gearman' do
        let(:params) { { gearman: true, gearman_encryption_key: 'super_secret' } }

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_class('nagiosxi::install') }
        it { is_expected.to contain_class('nagiosxi::ssl') }
        it { is_expected.to contain_class('nagiosxi::service') }
        it { is_expected.to contain_class('nagiosxi::gearman') }
      end

      context 'nagiosxi::install' do
        context 'with defaults' do
          it { is_expected.to contain_class('archive') }
          it do
            is_expected.to contain_archive('/opt/staging/nagiosxi/xi-latest.tar.gz')
              .that_notifies('Exec[nagiosxi-fullinstall]')
          end
          it do
            is_expected.to contain_archive('/opt/staging/nagiosxi/xi-latest.tar.gz').with(
              source:       'https://assets.nagios.com/downloads/nagiosxi/xi-latest.tar.gz',
              extract_path: '/opt/staging/nagiosxi',
              creates:      '/opt/staging/nagiosxi/nagiosxi',
            )
          end
          it do
            is_expected.to contain_exec('nagiosxi-fullinstall').with(
              command:     'fullinstall --non-interactive > nagiosxi-fullinstall.log',
              cwd:         '/opt/staging/nagiosxi/nagiosxi',
              refreshonly: true,
              timeout:     3600,
            )
          end
        end

        context 'with version specified' do
          let(:params) { { major: '2014', version: 'xi-2014r2.7' } }

          it do
            is_expected.to contain_archive('/opt/staging/nagiosxi/xi-2014r2.7.tar.gz').with(
              source: 'https://assets.nagios.com/downloads/nagiosxi/2014/xi-2014r2.7.tar.gz',
            )
          end
          it do
            is_expected.to contain_exec('nagiosxi-fullinstall').with(
              command:    'fullinstall --non-interactive > nagiosxi-fullinstall.log',
              cwd:        '/opt/staging/nagiosxi/nagiosxi',
              refreshonly: true,
            )
          end
        end

        context 'with action specified' do
          let(:params) { { action: 'upgrade' } }

          it do
            is_expected.to contain_archive('/opt/staging/nagiosxi/xi-latest.tar.gz').with(
              source: 'https://assets.nagios.com/downloads/nagiosxi/xi-latest.tar.gz',
            )
          end
          it do
            is_expected.to contain_exec('nagiosxi-upgrade').with(
              command:     'upgrade --non-interactive > nagiosxi-upgrade.log',
              cwd:         '/opt/staging/nagiosxi/nagiosxi',
              refreshonly: true,
            )
          end
        end
      end

      context 'nagiosxi::ssl' do
        context 'with defaults' do
          it { is_expected.not_to contain_file_line('nagiosxi_ssl_cert') }
          it { is_expected.not_to contain_file_line('nagiosxi_ssl_key') }
          it { is_expected.not_to contain_file_line('nagiosxi_ssl_chain') }
          it { is_expected.not_to contain_file('/etc/httpd/conf.d/rewrite_https.conf') }
          it { is_expected.not_to contain_file_line('nagiosxi_php_use_https') }
        end

        context 'with ssl set' do
          let(:params) do
            { ssl_cert:     '/file.crt',
              ssl_key:      '/file.key',
              ssl_chain:    '/file.chn',
              rewrite_https: true }
          end

          it { is_expected.to contain_file_line('nagiosxi_ssl_cert') }
          it { is_expected.to contain_file_line('nagiosxi_ssl_key') }
          it { is_expected.to contain_file_line('nagiosxi_ssl_chain') }
          it { is_expected.to contain_file('/etc/httpd/conf.d/rewrite_https.conf') }
          it do
            is_expected.to contain_file_line('nagiosxi_php_use_https').with(
              line:  "$cfg['use_https'] = true;",
              match: "^#?\\\$cfg\\['use_https'\\]",
            )
          end
        end
      end

      context 'nagiosxi::gearman' do
        context 'with defaults' do
          let(:params) { { gearman: true, gearman_encryption_key: 'super_secret' } }

          it { is_expected.to contain_class('nagiosxi::gearman::neb') }
          it { is_expected.to contain_class('nagiosxi::gearman::worker') }
          it { is_expected.to contain_package('gearmand') }
          it { is_expected.to contain_package('mod_gearman') }
          it { is_expected.to contain_file_line('nagiosxi_broker_mod_gearman') }
          it { is_expected.to contain_service('gearmand') }
          case facts[:operatingsystemrelease]
          when '6'
            it { is_expected.to contain_service('mod_gearman_worker') }
          when '7'
            it { is_expected.to contain_service('mod-gearman-worker') }
          end
        end
      end

      context 'nagiosxi::service' do
        context 'with defaults' do
          it { is_expected.to contain_class('apache::params') }
          it { is_expected.to contain_class('apache::service') }
          it { is_expected.to contain_service('nagios') }
        end
      end
    end
  end
end
