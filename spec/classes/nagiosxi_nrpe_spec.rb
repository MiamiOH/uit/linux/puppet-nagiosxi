require 'spec_helper'

describe 'nagiosxi::nrpe' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts.merge(environment: 'test', concat_basedir: '/var/lib/puppet/concat', staging_http_get: 'curl')
      end

      context 'with defaults' do
        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_class('nagiosxi::nrpe::install') }
      end

      context 'nagiosxi::nrpe::install' do
        context 'with defaults' do
          it { is_expected.to contain_class('archive') }
          it do
            is_expected.to contain_archive('/opt/staging/nagiosxi/linux-nrpe-agent.tar.gz')
              .that_notifies('Exec[nrpe-fullinstall]')
          end
          it do
            is_expected.to contain_archive('/opt/staging/nagiosxi/linux-nrpe-agent.tar.gz').with(
              source:       'https://assets.nagios.com/downloads/nagiosxi/agents/linux-nrpe-agent.tar.gz',
              extract_path: '/opt/staging/nagiosxi',
              creates:      '/opt/staging/nagiosxi/linux-nrpe-agent',
            )
          end
          it do
            is_expected.to contain_exec('nrpe-fullinstall').with(
              command:     "echo '127.0.0.1' | fullinstall --non-interactive > nrpe-fullinstall.log",
              cwd:         '/opt/staging/nagiosxi/linux-nrpe-agent',
              refreshonly: true,
              timeout:     3600,
            )
          end
        end

        context 'with allowed_hosts specified' do
          let(:params) { { allowed_hosts: ['127.0.0.1', '127.0.0.2'] } }

          it do
            is_expected.to contain_exec('nrpe-fullinstall').with(
              command: "echo '127.0.0.1 127.0.0.2' | fullinstall --non-interactive > nrpe-fullinstall.log",
            )
          end
        end
      end

      context 'nagiosxi::nrpe::config' do
        context 'with defaults' do
          it { is_expected.to contain_file_line('nrpe_xinetd_only_from') }
        end
      end

      context 'nagiosxi::nrpe::service' do
        context 'with defaults' do
          it { is_expected.to contain_service('xinetd') }
        end
      end
    end
  end
end
