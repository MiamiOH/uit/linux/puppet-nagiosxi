#
# == Definition: nagiosxi::config_entry
#
# Uses inifile to add/alter/remove options in nagiosxi
# configuation file (/usr/local/nagios/etc/nagios.cfg).
#
# TODO: use Augeas
#
# === Parameters
#
# [*name*]   - name of the parameter.
# [*ensure*] - present/absent. defaults to present.
# [*value*]  - value of the parameter.
#
# === Requires
#
# - Class['nagiosxi']
#
# === Examples
#
#   nagiosxi::config_entry { 'admin_email':
#     ensure => present,
#     value  => 'root@localhost',
#   }
#
#   nagiosxi::config_entry { 'admin_pager':
#     ensure => absent,
#   }
#
define nagiosxi::config_entry (
  $ensure = present,
  $value  = undef,
  $path   = '/usr/local/nagios/etc/nagios.cfg',
) {

  ini_setting { "nagiosxi-${name}":
    ensure            => $ensure,
    key_val_separator => '=',
    path              => $path,
    section           => '',
    setting           => $name,
    value             => $value,
  }

  Class['nagiosxi::install']
  -> Nagiosxi::Config_entry[$title]
  ~> Class['nagiosxi::service']
}
