# nrpe.pp
# Install / Configure linux-nrpe-agent for Nagios XI
# nagiosxi official install prevent installing from epel
#
# https://assets.nagios.com/downloads/nagiosxi/docs/Monitoring_Hosts_Using_NRPE.pdf
#
class nagiosxi::nrpe (
  Boolean $xinetd,
  String $config,
  String $include_dir,
  Hash $options,
  String $install_url,
  Enum['fullinstall'] $action,
  Boolean $logtofile,
  Optional[String] $proxy_server,
  Optional $environment,
  Array $allowed_hosts = ['127.0.0.1'],
) {

  validate_legacy(Stdlib::Absolutepath, 'validate_absolute_path', $config)
  validate_legacy(Stdlib::Absolutepath, 'validate_absolute_path', $include_dir)

  $allowed_hosts_join = join($allowed_hosts, ' ')

  contain 'nagiosxi::nrpe::install'
  contain 'nagiosxi::nrpe::config'
  contain 'nagiosxi::nrpe::service'

  Class['nagiosxi::nrpe::install']
  -> Class['nagiosxi::nrpe::config']
  ~> Class['nagiosxi::nrpe::service']
}
