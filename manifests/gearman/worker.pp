# worker.pp
# Install / Configure Mod-Gearman Worker for Nagios XI
# Core uses nagiosxi with gearman param
#
# https://assets.nagios.com/downloads/nagiosxi/docs/Integrating_Mod_Gearman_with_Nagios_XI.pdf
#
class nagiosxi::gearman::worker (
  String $master_ip,
  String $encryption_key,
  String $version,
  String $service_name,
  Optional $environment,
  Optional[String] $proxy_server,
  Boolean $logtofile,
  String $install_url,
) {

  unless $install_url == 'skip' {
    contain 'nagiosxi::gearman::worker::install'
    Class['nagiosxi::gearman::worker::install']
    -> Class['nagiosxi::gearman::worker::config']
  }

  contain 'nagiosxi::gearman::worker::config'
  contain 'nagiosxi::gearman::worker::service'

  Class['nagiosxi::gearman::worker::config']
  ~> Class['nagiosxi::gearman::worker::service']
}
