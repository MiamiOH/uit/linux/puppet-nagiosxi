# gearman/neb/service.pp
# Services on Mod-Gearman Neb With Nagios XI (on the core XI server)
#
class nagiosxi::gearman::neb::service {

  service { 'gearmand':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }
}
