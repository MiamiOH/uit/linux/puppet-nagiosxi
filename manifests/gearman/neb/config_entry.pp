#
# == Definition: nagiosxi::gearman::neb::config_entry
#
# Uses inifile to add/alter/remove options in mod_gearman neb
# configuation file (/etc/mod_gearman/mod_gearman_neb.conf).
#
# TODO: use Augeas
#
# === Parameters
#
# [*name*]   - name of the parameter.
# [*ensure*] - present/absent. defaults to present.
# [*value*]  - value of the parameter.
#
# === Requires
#
# - Class['nagiosxi::gearman::neb']
#
# === Examples
#
#   nagiosxi::gearman::neb::config_entry { 'server':
#     ensure => present,
#     value  => 'localhost:4730',
#   }
#
#   nagiosxi::gearman::neb::config_entry { 'dupserver':
#     ensure => absent,
#   }
#
define nagiosxi::gearman::neb::config_entry (
  $ensure = present,
  $value  = undef,
  $path   = '/etc/mod_gearman/mod_gearman_neb.conf',
) {

  ini_setting { "mod_gearman_neb-${name}":
    ensure            => $ensure,
    key_val_separator => '=',
    path              => $path,
    section           => '',
    setting           => $name,
    value             => $value,
  }

  Class['nagiosxi::gearman::neb::install']
  -> Nagiosxi::Gearman::Neb::Config_entry[$title]
  ~> Class['nagiosxi::gearman::neb::service']
}
