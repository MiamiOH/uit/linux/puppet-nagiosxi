# gearman/neb/install.pp
# Install Mod-Gearman Neb With Nagios XI (on the core XI server)
#
class nagiosxi::gearman::neb::install {

  package { 'gearmand':
    ensure => $nagiosxi::gearmand_version,
  }

  -> package { 'mod_gearman':
    ensure   => $nagiosxi::gearman::neb::gearman_neb_version,
    provider => 'rpm',
    source   => "${nagiosxi::gearman::neb::install_url}/mod_gearman-${nagiosxi::gearman::neb::gearman_neb_version}.x86_64.rpm",
  }
}
