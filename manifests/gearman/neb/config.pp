# gearman/neb/config.pp
# Configure Mod-Gearman Neb With Nagios XI (on the core XI server)
#
class nagiosxi::gearman::neb::config (
  String $broker_module,
  String $broker_config,
) {
  file_line { 'nagiosxi_broker_mod_gearman':
    path  => '/usr/local/nagios/etc/nagios.cfg',
    line  => "broker_module=/usr/lib64/mod_gearman/${broker_module} config=/etc/mod_gearman/${broker_config} eventhandler=no",
    match => '^#?broker_module=/usr/lib(64)?/mod_gearman/mod_gearman',
  }

  nagiosxi::gearman::neb::config_entry {
    'encryption': value  => 'yes';
    'key':        value  => $nagiosxi::gearman::neb::encryption_key;
  }
}
