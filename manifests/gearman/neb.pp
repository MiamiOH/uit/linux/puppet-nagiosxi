# neb.pp
# Install / Configure Mod-Gearman Neb With Nagios XI (on the core XI server)
# Workers use nagiosxi::gearman::worker
#
# https://assets.nagios.com/downloads/nagiosxi/docs/Integrating_Mod_Gearman_with_Nagios_XI.pdf
#
class nagiosxi::gearman::neb (
  Optional[String] $encryption_key,
  String $install_url,
  String $gearman_neb_version,
){

  contain 'nagiosxi::gearman::neb::install'
  contain 'nagiosxi::gearman::neb::config'
  contain 'nagiosxi::gearman::neb::service'

  Class['nagiosxi::gearman::neb::install']
  -> Class['nagiosxi::gearman::neb::config']
  ~> Class['nagiosxi::gearman::neb::service']
}
