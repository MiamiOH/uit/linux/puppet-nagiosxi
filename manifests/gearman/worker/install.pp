# gearman/worker/install.pp
# Install Mod-Gearman Worker for Nagios XI
#
class nagiosxi::gearman::worker::install (
  Stdlib::Absolutepath $staging_path,
  Optional[String] $cmd_options,
) inherits nagiosxi::gearman::worker {

  include 'archive'

  $shell_cmd     = "${nagiosxi::gearman::worker::version}.sh"
  $shell_log     = "${nagiosxi::gearman::worker::version}.log"
  $deployed_path = "${staging_path}/${module_name}"
  $command       = $nagiosxi::gearman::worker::logtofile ? {
    true  => "echo '${nagiosxi::gearman::worker::master_ip}' | ${shell_cmd} ${cmd_options} > ${shell_log}",
    false => "echo '${nagiosxi::gearman::worker::master_ip}' | ${shell_cmd} ${cmd_options}",
  }

  archive { "${deployed_path}/${shell_cmd}":
    source       => "${nagiosxi::gearman::worker::install_url}/scripts/${shell_cmd}",
    user         => 'root',
    group        => 'root',
    proxy_server => $nagiosxi::gearman::worker::proxy_server,
    notify       => [File["${deployed_path}/${shell_cmd}"], Exec[$shell_cmd]],
  }

  file{ "${deployed_path}/${shell_cmd}":
    ensure => file,
    mode   => '0755',
  }

  exec { $shell_cmd:
    command     => $command,
    cwd         => $deployed_path,
    environment => $nagiosxi::gearman::worker::environment,
    path        => "${::path}:${deployed_path}",
    refreshonly => true,
    timeout     => 3600, # 60 min
  }
}
