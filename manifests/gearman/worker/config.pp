# gearman/worker/config.pp
# Configure Mod-Gearman Worker for Nagios XI
#
class nagiosxi::gearman::worker::config(
  Stdlib::Absolutepath $path,
) {

  nagiosxi::gearman::worker::config_entry {
    'server':     value  => "${nagiosxi::gearman::worker::master_ip}:4730";
    'encryption': value  => 'yes';
    'key':        value  => $nagiosxi::gearman::worker::encryption_key;
  }
}
