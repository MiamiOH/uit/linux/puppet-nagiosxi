#
# == Definition: nagiosxi::gearman::worker::config_entry
#
# Uses inifile to add/alter/remove options in mod_gearman worker
# configuation file (/etc/mod_gearman/mod_gearman_worker.conf).
#
# TODO: use Augeas
#
# === Parameters
#
# [*name*]   - name of the parameter.
# [*ensure*] - present/absent. defaults to present.
# [*value*]  - value of the parameter.
#
# === Requires
#
# - Class['nagiosxi::gearman::worker']
#
# === Examples
#
#   nagiosxi::gearman::worker::config_entry { 'server':
#     ensure => present,
#     value  => "10.0.0.1:4730",
#   }
#
#   nagiosxi::gearman::worker::config_entry { 'identifier':
#     ensure => absent,
#   }
#
define nagiosxi::gearman::worker::config_entry (
  String $ensure                            = present,
  Optional[Variant[String, Integer]] $value = undef,
  Optional[Stdlib::Absolutepath] $path      = undef,
) {

  $_path = pick($path, $nagiosxi::gearman::worker::config::path)

  ini_setting { "mod_gearman_worker-${name}":
    ensure            => $ensure,
    key_val_separator => '=',
    path              => $_path,
    section           => '',
    setting           => $name,
    value             => $value,
  }

  if $nagiosxi::gearman::worker::install_url == 'skip' {
    Class['nagiosxi::gearman::neb::install']
    -> Nagiosxi::Gearman::Worker::Config_entry[$title]
  } else {
    Class['nagiosxi::gearman::worker::install']
    -> Nagiosxi::Gearman::Worker::Config_entry[$title]
  }

  Nagiosxi::Gearman::Worker::Config_entry[$title]
  ~> Class['nagiosxi::gearman::worker::service']
}
