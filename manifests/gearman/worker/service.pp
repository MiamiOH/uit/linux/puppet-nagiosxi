# gearman/worker/service.pp
# Services on Mod-Gearman Worker for Nagios XI
#
class nagiosxi::gearman::worker::service {

  service { $nagiosxi::gearman::worker::service_name:
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }
}
