# gearman.pp
# Install / Configure Mod-Gearman Neb and Worker With Nagios XI (on the core XI server)
#
# https://assets.nagios.com/downloads/nagiosxi/docs/Integrating_Mod_Gearman_with_Nagios_XI.pdf
#
class nagiosxi::gearman {

  class { 'nagiosxi::gearman::neb':
    encryption_key => $nagiosxi::gearman_encryption_key,
  }
  contain 'nagiosxi::gearman::neb'

  class { 'nagiosxi::gearman::worker':
    master_ip      => 'localhost',
    encryption_key => $nagiosxi::gearman_encryption_key,
    install_url    => 'skip',
    proxy_server   => $nagiosxi::proxy_server,
  }
  contain 'nagiosxi::gearman::worker'
}
