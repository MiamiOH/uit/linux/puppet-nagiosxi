# service.pp
# Manage NagiosXI service, which is currently apache
#
class nagiosxi::service {
  include 'apache::params'
  contain 'apache::service'

  service { $nagiosxi::services:
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }
}
