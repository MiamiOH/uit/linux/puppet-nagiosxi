# ssl.pp
# Configure ssl settings for NagiosXI
#
# https://assets.nagios.com/downloads/nagiosxi/docs/Configuring-SSL-with-Nagios%20XI.pdf
#
class nagiosxi::ssl {

  File_line {
    path => '/etc/httpd/conf.d/ssl.conf',
  }

  if $nagiosxi::ssl_cert {
    file_line { 'nagiosxi_ssl_cert':
      line  => "SSLCertificateFile ${nagiosxi::ssl_cert}",
      match => '^#?SSLCertificateFile',
    }
  }

  if $nagiosxi::ssl_key {
    file_line { 'nagiosxi_ssl_key':
      line  => "SSLCertificateKeyFile ${nagiosxi::ssl_key}",
      match => '^#?SSLCertificateKeyFile',
    }
  }

  if $nagiosxi::ssl_chain {
    file_line { 'nagiosxi_ssl_chain':
      line  => "SSLCertificateChainFile ${nagiosxi::ssl_chain}",
      match => '^#?SSLCertificateChainFile',
    }
  }

  if $nagiosxi::rewrite_https {
    file { '/etc/httpd/conf.d/rewrite_https.conf':
      ensure  => file,
      group   => 'root',
      owner   => 'root',
      mode    => '0644',
      content => template("${module_name}/rewrite_https.erb"),
    }
    file_line { 'nagiosxi_php_use_https':
      path  => '/usr/local/nagiosxi/html/config.inc.php',
      line  => "\$cfg['use_https'] = true;",
      match => "^#?\\\$cfg\\['use_https'\\]",
    }
  }
}
