# install.pp
# Download to staging path and install
#
# https://library.nagios.com/library/products/nagiosxi/downloads/main
# https://assets.nagios.com/downloads/nagiosxi/versions.php
# https://assets.nagios.com/downloads/nagiosxi/docs/XI_Manual_Installation_Instructions.pdf
# https://assets.nagios.com/downloads/nagiosxi/docs/XI_Upgrade_Instructions.pdf
#
class nagiosxi::install (
  Stdlib::Absolutepath $staging_path,
) {

  include 'archive'

  $archive_file  = "${nagiosxi::version}.tar.gz"
  $archive_url   = join(delete_undef_values([$nagiosxi::install_url, $nagiosxi::major, $archive_file]), '/')
  $deploy_path   = "${staging_path}/${module_name}"
  $deployed_path = "${deploy_path}/nagiosxi"
  $command       = $nagiosxi::logtofile ? {
    true  => "${nagiosxi::action} ${nagiosxi::options[$nagiosxi::action]} > nagiosxi-${nagiosxi::action}.log",
    false => "${nagiosxi::action} ${nagiosxi::options[$nagiosxi::action]}",
  }

  archive { "${deploy_path}/${archive_file}":
    source       => $archive_url,
    extract      => true,
    extract_path => $deploy_path,
    creates      => $deployed_path,
    proxy_server => $nagiosxi::proxy_server,
  }

  ~> exec { "nagiosxi-${nagiosxi::action}":
    command     => $command,
    cwd         => $deployed_path,
    environment => $nagiosxi::environment,
    path        => "${::path}:${deployed_path}",
    refreshonly => true,
    timeout     => 3600, # 60 min
  }
}
