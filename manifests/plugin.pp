# plugin.pp
# Install nagios plugins
# Fetch these plugins from:
#   the puppet master
#   the nagios exchange
#
# https://exchange.nagios.org/
# https://exchange.nagios.org/directory/Plugins
#
define nagiosxi::plugin (
  $ensure           = present,
  $content          = undef,
  $source           = undef,
  $staging_path     = '/opt/staging',
  $exchange_url     = undef,
  $exchange_link_id = undef,
  $exchange_cf_id   = undef,
  $exchange_base    = 'https://exchange.nagios.org/components/com_mtree/attachment.php',
  $libdir           = '/usr/local/nagios/libexec',
  $owner            = 'root',
  $group            = 'root',
  $mode             = '0755',
  $proxy_server     = undef,
) {

  $real_exchange_url = $exchange_url ? {
    undef   => "${exchange_base}?link_id=${exchange_link_id}&cf_id=${exchange_cf_id}",
    default => $exchange_url,
  }

  if $content or $source {
    # don't fail
    $real_source = $source
  } elsif $exchange_url or ($exchange_link_id and $exchange_cf_id) {
    include 'archive'

    $real_source = "${staging_path}/${module_name}/${title}"
    archive { $real_source:
      source       => $real_exchange_url,
      proxy_server => $proxy_server,
      before       => File["${libdir}/${title}"],
    }
  } else {
    fail("nagiosxi::plugin ${name} must specify one of (content, source, exchange_url)")
  }

  file { "${libdir}/${title}":
    ensure  => $ensure,
    content => $content,
    source  => $real_source,
    owner   => $owner,
    group   => $group,
    mode    => $mode,
  }

  if defined(Class['nagiosxi']) {
    Class['nagiosxi::install']
    -> Nagiosxi::Plugin[$title]
  }
  if defined(Class['nagiosxi::nrpe']) {
    Class['nagiosxi::nrpe::install']
    -> Nagiosxi::Plugin[$title]
  }
  if !defined(Class['nagiosxi']) and !defined(Class['nagiosxi::nrpe']) {
    fail("nagiosxi::plugin ${name} must include one of these classes first (nagiosxi, nagiosxi::nrpe)")
  }
}
