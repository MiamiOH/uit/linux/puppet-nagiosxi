# init.pp
# Main class of nagiosxi
# Declare main config here
#
# https://www.nagios.com/products/nagios-xi
#
class nagiosxi (
  String $install_url,
  Optional[String] $major,
  String $version,
  Enum['fullinstall', 'upgrade'] $action,
  Boolean $logtofile,
  Hash $options,
  String $libdir,
  Optional $environment,
  Optional[String] $proxy_server,
  Optional[String] $ssl_cert,
  Optional[String] $ssl_key,
  Optional[String] $ssl_chain,
  Boolean $rewrite_https,
  Boolean $gearman,
  Optional[String] $gearmand_version,
  Optional[String] $gearman_encryption_key,
  Stdlib::Absolutepath $staging_path,
  Array[String] $services,
) {

  $module_metadata            = load_module_metadata($module_name)
  $supported_operatingsystems = $module_metadata['operatingsystem_support']

  $supported_os_names = $supported_operatingsystems.map |$os| {
    $os['operatingsystem']
  }

  unless member($supported_os_names, $facts['os']['name']) {
    fail("${facts['os']['name']} not supported")
  }

  $supported_os_releases = $supported_operatingsystems.map |$os| {
    if $os['operatingsystem'] == $facts['os']['name'] { $os['operatingsystemrelease'] }
  }.flatten

  unless (member($supported_os_releases, $facts['os']['release']['major']) and $facts['os']['architecture'] == 'x86_64') {
    fail("${facts['os']['name']} ${facts['os']['release']['major']} ${facts['os']['architecture']} not supported.")
  }

  contain 'nagiosxi::install'
  contain 'nagiosxi::ssl'
  contain 'nagiosxi::service'

  Class['nagiosxi::install']
  -> Class['nagiosxi::ssl']
  ~> Class['nagiosxi::service']

  if $gearman {
    unless $gearman_encryption_key { fail('You must pass $gearman_encryption_key if $gearman') }

    contain 'nagiosxi::gearman'
    Class['nagiosxi::ssl']
    -> Class['nagiosxi::gearman']
    ~> Class['nagiosxi::service']
  }
}
