# nrpe/config.pp
# Configure nrpe With Nagios XI
#
class nagiosxi::nrpe::config {

  $include_dir = $nagiosxi::nrpe::include_dir

  file_line { 'nrpe_xinetd_only_from':
    path  => '/etc/xinetd.d/nrpe',
    line  => "\tonly_from\t= ${nagiosxi::nrpe::allowed_hosts_join}",
    match => '.*only_from.*',
  }

  -> file { $nagiosxi::nrpe::include_dir:
    ensure => directory,
  }

  -> file { $nagiosxi::nrpe::config:
    ensure  => file,
    content => template("${module_name}/nrpe.cfg.erb"),
  }
}
