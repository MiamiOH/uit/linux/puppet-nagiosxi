# nrpe/service.pp
# Services on nrpe for Nagios XI
#
# The source installer from nagiosxi uses xinetd
#
class nagiosxi::nrpe::service {

  ensure_resource('service', 'xinetd', {
      ensure     => running,
      enable     => true,
      hasrestart => true,
      hasstatus  => true,
  })
}
