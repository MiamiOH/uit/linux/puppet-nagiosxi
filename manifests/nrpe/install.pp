# nrpe/install.pp
# Install linux-nrpe-agent for Nagios XI
#
# https://assets.nagios.com/downloads/nagiosxi/docs/Installing_The_XI_Linux_Agent.pdf
#
class nagiosxi::nrpe::install (
  Stdlib::Absolutepath $staging_path,
) {

  include 'archive'

  $archive_file  = 'linux-nrpe-agent.tar.gz'
  $archive_url   = "${nagiosxi::nrpe::install_url}/agents/${archive_file}"
  $deploy_path   = "${staging_path}/${module_name}"
  $deployed_path = "${deploy_path}/linux-nrpe-agent"
  $command       = $nagiosxi::nrpe::logtofile ? {
    true  => "echo '${nagiosxi::nrpe::allowed_hosts_join}' | ${nagiosxi::nrpe::action} ${nagiosxi::nrpe::options[$nagiosxi::nrpe::action]} > nrpe-${nagiosxi::nrpe::action}.log", # lint:ignore:140chars
    false => "echo '${nagiosxi::nrpe::allowed_hosts_join}' | ${nagiosxi::nrpe::action} ${nagiosxi::nrpe::options[$nagiosxi::nrpe::action]}",
  }

  archive { "${deploy_path}/${archive_file}":
    source       => $archive_url,
    extract      => true,
    extract_path => $deploy_path,
    creates      => $deployed_path,
    proxy_server => $nagiosxi::nrpe::proxy_server,
  }

  ~> exec { "nrpe-${nagiosxi::nrpe::action}":
    command     => $command,
    cwd         => $deployed_path,
    environment => $nagiosxi::nrpe::environment,
    path        => "${::path}:${deployed_path}",
    refreshonly => true,
    timeout     => 3600, # 60 min
  }
}
