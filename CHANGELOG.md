# Changelog

All notable changes to this project will be documented in this file.

## Release 0.6.1

**Features**
    * Increase dependency limits
    * Remove absolute class names
    * Incorporate PDK

**Bugfixes**

**Known Issues**
